from flask import Flask
import redis
import json

app = Flask(__name__)

alumno = '{"curp":"prueba"}'
alumno_json = json.dumps(alumno)

@app.route("/")
def index():
	return alumno_json

@app.route("/calificaciones/alta/<curp>/")
def alta(curp):
	return "Has dado de alta las calificaciones de ..." + curp
	
@app.route("/calificaciones/modificar")
def modifica():
	return "Has modificado a..."

@app.route("/calificaciones/consulta")
def consulta():
	return "Las calificaciones de ... son"