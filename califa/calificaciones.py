from flask import Flask
import redis
import json
import requests

app = Flask(__name__)

alumno = '{"curp":"prueba"}'
alumno_json = json.dumps(alumno)

@app.route("/")
def index():
	return alumno_json


@app.route("/calificaciones/alta/<curp>/<ciclo>/<materia>/<bimestre>/<calif>/")
def alta(curp,ciclo,materia,bimestre,calif):
	r = redis.StrictRedis(host='localhost', port=6379, db=1)
	#alumno = {"curp":curp, "ciclo_escolar":"2017-2018","nombre":"Matematicas","calificacion":10,"bimestre":2}
	#r.set(alumno["curp"],ciclo["ciclo_escolar"],materia["nombre"],materia["calificacion"],materia["bimestre"])
	create = "CREATE (:alumno{curp:'"+curp+"'})-[:cursa]->(:ciclo{ciclo_escolar:'"+ciclo+"'})-[:califica]->(:materia{nombre:'"+materia+"',calificacion:'"+calif+"',bimestre:'"+bimestre+"'})"
	reply = r.execute_command('GRAPH.QUERY', 'califas', create)
	
	return "Has dado de alta las calificaciones de ..." + curp +" en el ciclo "+ciclo


@app.route("/calificaciones/consulta/<curp>")
def consultar_calificaciones(curp):
	alumno = requests.get('http://80.211.115.85/alumno/'+curp)
	
	r = redis.StrictRedis(host='localhost', port=6379, db=1)
	consulta = "MATCH (a:alumno)-[:cursa]->(c:ciclo)-[:califica]->(m:materia) WHERE a.curp='" + curp + "' return a,c,m"
	reply = r.execute_command('GRAPH.QUERY', 'califas', consulta)
	return "Las calificaciones son: " + str(reply)

@app.route("/calificaciones/modificar")
def modifica():
	return "Has modificado a..."

@app.route("/calificaciones/consulta")
def consulta():
	return "Las calificaciones de ... son"