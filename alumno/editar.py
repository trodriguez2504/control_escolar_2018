from flask import Flask
app = Flask(__name__)
import redis

@app.route("/alumno/editar")
def editar():
	r = redis.StrictRedis(host='localhost', port =6379, db=0)
	r.set("CURP12345678901234:nombre","Itzel")
	r.set("CURP12345678901234:primer_apellido","Roman")
	r.set("CURP12345678901234:segundo_apellido","Natera")

	return "editado"	
